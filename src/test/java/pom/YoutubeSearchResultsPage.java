package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class YoutubeSearchResultsPage extends YoutubeBasePage{

    @FindBy(xpath = "/html/body/ytd-app/div[1]/ytd-page-manager/ytd-search/div[1]/ytd-two-column-search-results-renderer/div/ytd-section-list-renderer/div[2]/ytd-item-section-renderer/div[3]/ytd-video-renderer[1]/div[1]/div/div[1]/div/h3/a/yt-formatted-string")
    WebElement FirstVideoTitle;

    @FindBy(id = "video-title")
    WebElement FirstVideoThumbnail;

    @FindBy(id = "channel-thumbnail")
    WebElement FirstVideoUploader;

    public YoutubeSearchResultsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(decorator, this);
    }

    public void checkFirstVideoTitle(){
        Assert.assertTrue(containsIgnoreCase(FirstVideoTitle.getAttribute("aria-label"),"despacito"));
    }

    public void clickFirstVideo() {
        FirstVideoThumbnail.click();
    }

    public void clickFirstVideoUploader() {
        FirstVideoUploader.click();
    }
}

package pom;

import io.github.sukgu.support.ElementFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class YoutubeBasePage {

    public static String baseURL = "https://www.youtube.com/";

    WebDriver driver;
    ElementFieldDecorator decorator;

    @FindBy(xpath = "/html/body/ytd-app/ytd-consent-bump-v2-lightbox/tp-yt-paper-dialog/div[4]/div[2]/div[6]/div[1]/ytd-button-renderer[2]/yt-button-shape/button/yt-touch-feedback-shape/div/div[2]")
    WebElement AcceptCookiesButton;

    @FindBy(id = "button")
    WebElement SidebarMenu;

    @FindBy(name = "search_query")
    WebElement SearchBar;

    @FindBy(xpath = "/html/body/ytd-app/div[1]/div/ytd-masthead/div[4]/div[3]/div[2]/ytd-button-renderer/yt-button-shape/a")
    WebElement SignInButton;

    public YoutubeBasePage(WebDriver driver) {
        this.driver = driver;
        decorator = new ElementFieldDecorator(new DefaultElementLocatorFactory(driver));
        PageFactory.initElements(decorator, this);
    }

    public void clickAcceptCookies() {
        AcceptCookiesButton.click();
    }

    public void clickSignInButton() {
        SignInButton.click();
    }
}

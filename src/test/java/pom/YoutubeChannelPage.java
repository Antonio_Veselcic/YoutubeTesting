package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class YoutubeChannelPage extends YoutubeBasePage{

    @FindBy(id = "img")
    WebElement ChannelLogo;

    @FindBy(id = "text-container")
    WebElement Username;

    @FindBy(id = "wrapper")
    WebElement AboutButton;

    @FindBy(id = "tabsContent")
    WebElement ChannelTabs;

    public YoutubeChannelPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(decorator, this);
    }

    public boolean isChannelLogoVisible() {
        return ChannelLogo.isDisplayed();
    }

    public void checkChannelLogo() {
        Assert.assertTrue(isChannelLogoVisible());
    }

    public void checkUsername() {
        Assert.assertTrue(Username.isDisplayed());
    }

    public void checkAboutButton() {
        Assert.assertTrue(AboutButton.isDisplayed());
    }

    public void checkChannelTabs() {
        Assert.assertTrue(ChannelTabs.isDisplayed());
    }
}

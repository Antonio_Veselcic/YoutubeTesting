package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class YoutubeVideoPage extends YoutubeBasePage {

    @FindBy(id = "movie_player")
    WebElement VideoPlayer;

    @FindBy(id = "snippet")
    WebElement Description;

    @FindBy(id = "commnets")
    WebElement Comments;

    public YoutubeVideoPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean isVideoLoaded() {
        return VideoPlayer.isDisplayed();
    }

    public void checkVideoPlayer() {
        Assert.assertTrue(VideoPlayer.isDisplayed());
    }

    public void checkDescription() {
        Assert.assertTrue(Description.isDisplayed());
    }
}

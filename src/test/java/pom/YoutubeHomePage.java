package pom;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class YoutubeHomePage extends YoutubeBasePage{

    @FindBy(id = "scroll-container")
    WebElement ScrolableTopics;

    @FindBy(id = "contents")
    WebElement RecommendedVideos;

    public YoutubeHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(decorator, this);
    }

    public void checkSidebarMenu() {
        Assert.assertFalse(SidebarMenu.isDisplayed());
    }

    public void checkScrollableTopics() {
        Assert.assertTrue(ScrolableTopics.isDisplayed());
    }

    public void checkRecommendedVideos() {
        Assert.assertTrue(RecommendedVideos.isDisplayed());
    }

    public void searchForVideo() {
        SearchBar.click();
        SearchBar.sendKeys("despacito");
        SearchBar.sendKeys(Keys.ENTER);
    }
}

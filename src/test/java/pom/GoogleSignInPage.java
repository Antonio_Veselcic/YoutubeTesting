package pom;

import io.github.sukgu.support.ElementFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.testng.Assert;

public class GoogleSignInPage{

    WebDriver driver;
    ElementFieldDecorator decorator;

    @FindBy(id = "headingText")
    WebElement SignInText;

    @FindBy(id = "identifierId")
    WebElement EmailOrPhoneTextInput;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/c-wiz/div/div[2]/div/div[2]/div/div[2]/div/div/div[1]/div/button/span")
    WebElement CreateAccountButton;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/c-wiz/div/div[2]/div/div[2]/div/div[1]/div/div/button/span")
    WebElement NextButton;

    public GoogleSignInPage(WebDriver driver) {
        this.driver = driver;
        decorator = new ElementFieldDecorator(new DefaultElementLocatorFactory(driver));
        PageFactory.initElements(decorator, this);
    }

    public void checkSignInText() {
        Assert.assertTrue(SignInText.isDisplayed());
    }

    public void checkEmailOrPhoneTextInput() {
        Assert.assertTrue(EmailOrPhoneTextInput.isDisplayed());
    }

    public void checkCreateAccountButton() {
        Assert.assertTrue(CreateAccountButton.isDisplayed());
    }

    public void checkNextButton() {
        Assert.assertTrue(NextButton.isDisplayed());
    }
}

package test;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import pom.*;
import org.openqa.selenium.support.ui.Wait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class YoutubeTest {

    WebDriver driver;
    Wait<WebDriver> wait;
    YoutubeHomePage youtubeHomePage;
    YoutubeSearchResultsPage youtubeSearchResultsPage;
    Capabilities capabilities;

    @BeforeTest
    @Parameters("browser")
    public void setup(String browser) throws MalformedURLException {

        if(browser.equalsIgnoreCase("chrome")){
            capabilities = new ChromeOptions();
        }
        else if(browser.equalsIgnoreCase("firefox")){
            capabilities = new FirefoxOptions();
        }

        driver = new RemoteWebDriver(new URL("http://localhost:4444/"), capabilities);
        wait = new WebDriverWait(driver, 5);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void redirectToHomePage() {
        driver.navigate().to(YoutubeBasePage.baseURL);
    }

    @Test
    public void test1_HomePage(){

        youtubeHomePage = new YoutubeHomePage(driver);

        youtubeHomePage.clickAcceptCookies();
        youtubeHomePage.checkSidebarMenu();
        youtubeHomePage.checkScrollableTopics();
        youtubeHomePage.checkRecommendedVideos();
    }

    @Test
    public void test2_SearchResultsPage() {

        youtubeHomePage = new YoutubeHomePage(driver);
        youtubeSearchResultsPage = new YoutubeSearchResultsPage(driver);

        youtubeHomePage.searchForVideo();
        youtubeSearchResultsPage.checkFirstVideoTitle();
    }

    @Test
    public void test3_ChannelPage() throws InterruptedException {

        YoutubeHomePage youtubeHomePage = new YoutubeHomePage(driver);
        YoutubeSearchResultsPage youtubeSearchResultsPage = new YoutubeSearchResultsPage(driver);
        YoutubeChannelPage youtubeChannelPage = new YoutubeChannelPage(driver);

        youtubeHomePage.searchForVideo();
        youtubeSearchResultsPage.clickFirstVideoUploader();

        wait.until(it -> youtubeChannelPage.isChannelLogoVisible());

        youtubeChannelPage.checkChannelLogo();
        youtubeChannelPage.checkUsername();
        youtubeChannelPage.checkAboutButton();
        youtubeChannelPage.checkChannelTabs();
    }

    @Test
    public void test4_VideoPage() {

        YoutubeSearchResultsPage youtubeSearchResultsPage = new YoutubeSearchResultsPage(driver);
        YoutubeVideoPage youtubeVideoPage = new YoutubeVideoPage(driver);

        driver.navigate().back();

        youtubeSearchResultsPage.clickFirstVideo();
        wait.until(it -> youtubeVideoPage.isVideoLoaded());

        youtubeVideoPage.checkVideoPlayer();
        youtubeVideoPage.checkDescription();
    }

    @Test
    public void test5_GoogleSignInPage() {

        YoutubeVideoPage youtubeVideoPage = new YoutubeVideoPage(driver);
        GoogleSignInPage googleSignInPage = new GoogleSignInPage(driver);

        youtubeVideoPage.clickSignInButton();

        googleSignInPage.checkSignInText();
        googleSignInPage.checkEmailOrPhoneTextInput();
        googleSignInPage.checkCreateAccountButton();
        googleSignInPage.checkNextButton();
    }

    @AfterTest
    public void teardownTest() {
        driver.quit();
    }
}
















































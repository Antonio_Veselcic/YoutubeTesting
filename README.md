# Youtube Web UI Test Project

## Running tests

Required tools:
<ol>
    <li><a href="https://www.oracle.com/java/technologies/downloads/" target="_blank">Java</a> (version 11+)</li>
    <li><a href="https://github.com/SeleniumHQ/selenium/releases/download/selenium-4.16.0/selenium-server-4.16.1.jar" target="_blank">Selenium Server/Grid</a></li>
</ol>

Instructions:
<ol>
  <li>Clone this repository</li>
  <li>In the command line, navigate to the folder containing Selenium Server file is.</li>
  <li>Copy the following code and click ENTER:</li>

  ```
  java -jar selenium-server-4.16.1.jar standalone --selenium-manager true
  ```

  <li>Open this project in your IDE, then go to terminal(navigate to project folder), copy the following line and click CTRL+ENTER:</li>

  ```
  mvn test
  ```

 <li>Wait for tests to finish.</li>
 <li>In your IDE, inside the project folder, navigate to target/surefire-reports/index.html, right click it and open in your preferred browser. From there, you can see tests results.</li>
 <li>Once you are done, it is recommended to go to opened command line and terminate Selenium Server by clicking CTRL + C</li>
</ol>

## Test descriptions

### Testing YouTube Homepage - method test1_HomePage

This test is the first one and therefore starts by accepting cookies so the other tests are able to run. Tests checks for the sidebar menu, for the scrollable elements containing video topics and most importantly for the recommended videos section.

### Testing YouTube Search Result Page - method test2_SearchResultsPage

This test starts from the homepage and searches for a video. Once the results are available the tests checks that the title contains the user searched for.

### Testing YouTube Channel Page - method test3_ChannelPage

This test ensures the channel page contains all the required elements. These elements are channel logo, channel username, "about channel" button and channel tabs.

### Testing YouTube Video Player Page - method test4_VideoPage

This test checks the page playing a video. It checks that the video element is rendered with the appropriate description.

### Testing Google Sign In Page - method test5_GoogleSignInPage

This test checks that clicking the sing in button leads to the Google account sign in page and that the opened page contains proper input elements. 

## Used technologies and techniques

Here is a list of technologies and techniques used to create this project. More can be learned by reading linked articles.
<ul>
    <li><a href="https://www.selenium.dev/documentation/webdriver/">Selenium Web Driver</a></li>
    <li><a href="https://www.selenium.dev/documentation/grid/">Selenium Grid</a></li>
    <li><a href="https://www.selenium.dev/documentation/webdriver/waits/">Selenium Waiting Strategies - Implicit and Explicit wait</a></li>
    <li><a href="https://www.browserstack.com/guide/page-object-model-in-selenium">Page Object Model (Page Factory)</a></li>
    <li><a href="https://www.browserstack.com/guide/cross-browser-testing-in-selenium">Cross-Browser testing</a></li>
    <li><a href="https://maven.apache.org/surefire/maven-surefire-report-plugin/index.html">Surefire - Test reporting</a></li>
    <li><a href="https://git-scm.com/docs/gitignore">.gitignore</a></li>
</ul>